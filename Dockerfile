FROM ruby:2.7

LABEL vendor="hands development ab" \
      description=" " \
      maintainer="Harald"

USER root

RUN apt-get update && apt-get upgrade && apt-get clean

COPY contrib/Gemfile*    /tmp/

RUN cd tmp && bundle install






